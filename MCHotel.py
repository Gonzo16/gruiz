import numpy as np
from itertools import repeat
import json

N = 50000
max_rooms = 70

dicc = {'price': [70, 80, 90, 100, 110, 120, 140, 160],
        'booking rate': [40, 34, 30, 27, 24, 22, 18, 15]}

lista_p = dicc['price']
lista_b = dicc['booking rate']

print(lista_p)


def calc_revenue_v(q: int, demand: int, p: int) -> int:
    revenue = p * min(q, demand)
    return revenue


def monte_carlo_v():
    for q in range(0, max_rooms + 1):
        ingresos = {}
        for p in lista_p:
            demand = np.maximum(np.random.poisson(q, 50000), 0)
            revenue = map(calc_revenue_v, repeat(q, N), demand, repeat(p, N))
            est_revenue = sum(revenue) / N
            ingresos.update({p: est_revenue})
        print(ingresos)
    return ingresos


if __name__ == '__main__':
    strategy_v = monte_carlo_v()
    bestq = max(strategy_v, key=strategy_v.get)
    print(strategy_v)
    with open('strategy_v.json', 'w') as f:
        json.dump(strategy_v, f, sort_keys=True, indent=4)
