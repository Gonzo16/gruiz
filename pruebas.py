import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import logging

N = 50000
q = 70
revenueT = 0
revenueF = 0

dicc = {'price': [70, 80, 90, 100, 110, 120, 140, 160],
        'booking rate': [40, 34, 30, 27, 24, 22, 18, 15]}

lista_p = dicc['price']
lista_b = dicc['booking rate']


def static_model(p: int, demand: int, counter: int) -> int:
    if counter == 0:
        global revenueT
        revenueT = p * min(q, demand)
        print("Expected revenue for Thursday:", revenueT)
    else:
        global revenueF
        revenueF = p * min(q, demand)
        print("Expected revenue for Friday:", revenueF)


def validar_precio(precio: int) -> int:
    if 70 <= precio <= 160:
        return 0
    else:
        return -1


def calc_revenue_tot(thursday: int, friday: int) -> int:
    return thursday + friday


def calc_rooms(habs: int):
    for i in range(0, len(lista_b)):
        if habs >= lista_b[i]:
            habs = 0
            lam = lista_b[i]
    return lam


def calc_demanda_jueves() -> int:
    counter = 0
    x = -1
    while x != 0:
        print("Please introduce the desired price: ")
        price_j = int(input())
        x = validar_precio(price_j)
    for i in range(0, len(lista_p)):
        if price_j == lista_p[i]:
            lam = lista_b[i]
            demand_j = np.random.poisson(lam, 50000)
            media_demand_j = round(np.mean(demand_j))
            # print(demand)
            print("Number of rooms demanded for that price:", media_demand_j)
            plt.hist(demand_j, 55, density=True)
            plt.show()
            static_model(price_j, media_demand_j, counter)
    return q-media_demand_j


def calc_demanda_viernes(rooms: int) -> int:
    counter = 1
    lam = calc_rooms(rooms)
    for i in range(0, len(lista_p)):
        if lam == lista_b[i]:
            price_v = lista_p[i]
            print("Optimized price per room for Friday:", price_v)
    demand_v = np.random.poisson(lam, 50000)
    media_demand_v = round(np.mean(demand_v))
    # print(demand)
    print("Number of rooms predicted for Friday:", media_demand_v)
    plt.hist(demand_v, 55, density=True)
    plt.show()
    static_model(price_v, media_demand_v, counter)


print("Thursday")
rooms = calc_demanda_jueves()
print("-------------------------------------")
print("Friday")
calc_demanda_viernes(rooms)
print("-------------------------------------")
revenue_total = calc_revenue_tot(revenueT, revenueF)
print("Expected total revenue:", revenue_total)
